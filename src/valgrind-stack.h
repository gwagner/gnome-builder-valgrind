#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define VALGRIND_TYPE_STACK (valgrind_stack_get_type())

G_DECLARE_FINAL_TYPE (ValgrindStack, valgrind_stack, VALGRIND, STACK, GObject)

ValgrindStack *valgrind_stack_new        (const gchar   *ip,
                                          const gchar   *object);
void           valgrind_stack_set_ip     (ValgrindStack *self,
                                          const gchar   *ip);
gchar         *valgrind_stack_get_ip     (ValgrindStack *self);
void           valgrind_stack_set_fn     (ValgrindStack *self,
                                          const gchar   *fn);
gchar         *valgrind_stack_get_fn     (ValgrindStack *self);
void           valgrind_stack_set_object (ValgrindStack *self,
                                          const gchar   *object);
gchar         *valgrind_stack_get_object (ValgrindStack *self);

G_END_DECLS
