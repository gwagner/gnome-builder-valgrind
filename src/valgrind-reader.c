/* valgrind-reader.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gio/gio.h>
#include "valgrind-resources.h"
#include "valgrind-finding.h"
#include "valgrind-category.h"
#include <libxml/parser.h>

void
parse_frame (xmlNodePtr       node,
             ValgrindFinding *finding)
{
  g_autofree gchar *ip = NULL;
  g_autofree gchar *object = NULL;
  g_autofree gchar *fn = NULL;
  for (xmlNodePtr cur = node->children; cur; cur = cur->next)
    {
      if (g_strcmp0 ((gchar *)cur->name, "ip") == 0)
        {
          ip = g_strdup ((const gchar *)cur->children->content);
        }
      else if (g_strcmp0 ((gchar *)cur->name, "obj") == 0)
        {
          object = g_strdup ((const gchar *)cur->children->content);
        }
      else if (g_strcmp0 ((gchar *)cur->name, "fn") == 0)
        {
          fn = g_strdup ((const gchar *)cur->children->content);
        }
    }
    
  ValgrindStack *stack = valgrind_stack_new (ip, object);
  if (fn != NULL)
    valgrind_stack_set_fn (stack, fn);
  valgrind_finding_add_stack (finding, stack);
  
}

void
parse_stack (xmlNodePtr       node,
             ValgrindFinding *finding)
{
  for (xmlNodePtr cur = node->children; cur; cur = cur->next)
    {
      if (g_strcmp0 ((gchar *) cur->name, "frame") == 0)
        {
          parse_frame (cur, finding);
        }
    }
}

void
parse_xwhat (xmlNodePtr       node,
             ValgrindFinding *finding)
{
  for (xmlNodePtr cur = node->children; cur; cur = cur->next)
    {
      if (g_strcmp0 ((gchar *) cur->name, "leakedbytes") == 0)
        {
          gint leaked_bytes = strtol ((const gchar *) cur->children->content, NULL, 10);
          valgrind_finding_set_leaked_bytes (finding, leaked_bytes);
        }
    }
}

void
parse_error (xmlNodePtr  node,
             GHashTable *categories)
{
  ValgrindFinding *finding;
  xmlNodePtr stack = NULL;
  xmlNodePtr xwhat = NULL;

  for (xmlNodePtr cur = node->children; cur; cur = cur->next)
    {
      if (g_strcmp0 ((const gchar *) cur->name, "kind") == 0)
        {
          gchar *kind = (gchar *) cur->children->content;
          ValgrindCategory *category;

          if (g_hash_table_contains (categories, kind))
            {
              category = g_hash_table_lookup (categories, kind);
            }
          else
            {
              category = valgrind_category_new (kind);
              g_hash_table_insert (categories, kind, category);
            }

          finding = valgrind_finding_new ();
          valgrind_category_add_finding (category, finding);
        }
      else if (g_strcmp0 ((const gchar *) cur->name, "stack") == 0)
        {
          stack = cur;
        }
      else if (g_strcmp0 ((const gchar *) cur->name, "xwhat") == 0)
        {
          xwhat = cur;
        }
    }
  
  parse_xwhat (xwhat, finding);
  parse_stack (stack, finding);
}

void
read_errors (xmlNodePtr  node,
             GHashTable *categories)
{

  for (xmlNodePtr cur = node; cur; cur = cur->next)
    {
      if (cur->type == XML_ELEMENT_NODE)
        {
          if (g_strcmp0 ((gchar *)cur->name, "error") == 0)
            {
              parse_error (cur, categories);
            }
          else {
            read_errors (cur->children, categories);
          }
        }
    }
}

GPtrArray * 
valgrind_read ()
{
  GPtrArray *arr = g_ptr_array_new ();
  g_autoptr(GHashTable) categories = g_hash_table_new (g_str_hash, g_str_equal);

  GResource *resource = valgrind_get_resource ();
  GBytes *example_output_bytes = g_resource_lookup_data (resource, "/org/gnome/valgrind/example-output.xml", G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  gsize example_output_size;
  gchar *example_output = (gchar *)g_bytes_get_data (example_output_bytes, &example_output_size);

  xmlDocPtr doc = xmlParseMemory (example_output, example_output_size);
  xmlNodePtr root = xmlDocGetRootElement (doc);
  read_errors (root, categories);

  GList *values = g_hash_table_get_values (categories);
  for (GList *cur = values; cur; cur = cur->next)
    {
      g_ptr_array_add (arr, cur->data);
    }
  xmlFreeDoc (doc);
  g_bytes_unref (example_output_bytes);

  return arr;
}
