#include "valgrind-stack.h"

struct _ValgrindStack
{
  GObject parent_instance;
  
  gchar *ip;
  gchar *fn;
  gchar *object;
};

G_DEFINE_TYPE (ValgrindStack, valgrind_stack, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_IP,
  PROP_FN,
  PROP_OBJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

ValgrindStack *
valgrind_stack_new (const gchar *ip, const gchar *object)
{
  return g_object_new (VALGRIND_TYPE_STACK,
                       "ip", ip,
                       "object", object,
                       NULL);
}

static void
valgrind_stack_finalize (GObject *object)
{
  ValgrindStack *self = (ValgrindStack *)object;

  g_free (self->ip);
  g_free (self->fn);
  g_free (self->object);

  G_OBJECT_CLASS (valgrind_stack_parent_class)->finalize (object);
}

static void
valgrind_stack_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  ValgrindStack *self = VALGRIND_STACK (object);

  switch (prop_id)
    {
    case PROP_IP:
      g_value_set_string (value, valgrind_stack_get_ip (self));
      break;
    case PROP_FN:
      g_value_set_string (value, valgrind_stack_get_fn (self));
      break;
    case PROP_OBJECT:
      g_value_set_string (value, valgrind_stack_get_object (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
valgrind_stack_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  ValgrindStack *self = VALGRIND_STACK (object);

  switch (prop_id)
    {
    case PROP_IP:
      valgrind_stack_set_ip (self, g_value_get_string (value));
      break;
    case PROP_FN:
      valgrind_stack_set_fn (self, g_value_get_string (value));
      break;
    case PROP_OBJECT:
      valgrind_stack_set_object (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
valgrind_stack_class_init (ValgrindStackClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = valgrind_stack_finalize;
  object_class->get_property = valgrind_stack_get_property;
  object_class->set_property = valgrind_stack_set_property;
  
  properties [PROP_IP] =
    g_param_spec_string ("ip",
                         "Ip",
                         "the instruction pointer",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
  
  properties [PROP_FN] =
    g_param_spec_string ("fn",
                         "Fn",
                         "Fn",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
  
  properties [PROP_OBJECT] =
    g_param_spec_string ("object",
                         "Object",
                         "the object of the memleak",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
  
  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
valgrind_stack_init (ValgrindStack *self)
{
}

void
valgrind_stack_set_ip (ValgrindStack *self,
                       const gchar   *ip)
{
  g_return_if_fail (VALGRIND_IS_STACK (self));
  
  if (g_strcmp0 (self->ip, ip) != 0)
    {
      g_free (self->ip);
      self->ip = g_strdup (ip);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_IP]);
    }
}

gchar *
valgrind_stack_get_ip (ValgrindStack *self)
{
  g_return_val_if_fail (VALGRIND_IS_STACK (self), NULL);
  
  return self->ip;
}

void           
valgrind_stack_set_fn (ValgrindStack *self,
                       const gchar   *fn)
{
  g_return_if_fail (VALGRIND_IS_STACK (self));
  
  if (g_strcmp0 (self->fn, fn) != 0)
    {
      g_free (self->fn);
      self->fn = g_strdup (fn);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FN]);
    }
}

gchar *
valgrind_stack_get_fn (ValgrindStack *self)
{
  g_return_val_if_fail (VALGRIND_IS_STACK (self), NULL);
  
  return self->fn;
}

void 
valgrind_stack_set_object (ValgrindStack *self,
                           const gchar   *object)
{
  g_return_if_fail (VALGRIND_IS_STACK (self));
  
  if (g_strcmp0 (self->object, object) != 0)
    {
      g_free (self->object);
      self->object = g_strdup (object);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_OBJECT]);
    }
}

gchar *
valgrind_stack_get_object (ValgrindStack *self)
{
  g_return_val_if_fail (VALGRIND_IS_STACK (self), NULL);
  
  return self->object;
}
