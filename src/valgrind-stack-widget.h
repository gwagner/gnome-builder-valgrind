#pragma once

#include <gtk/gtk.h>
#include "valgrind-stack.h"

G_BEGIN_DECLS

#define VALGRIND_TYPE_STACK_WIDGET (valgrind_stack_widget_get_type())

G_DECLARE_FINAL_TYPE (ValgrindStackWidget, valgrind_stack_widget, VALGRIND, STACK_WIDGET, GtkBox)

ValgrindStackWidget *valgrind_stack_widget_new       (ValgrindStack       *stack);
void                 valgrind_stack_widget_set_stack (ValgrindStackWidget *self,
                                                      ValgrindStack       *stack);

G_END_DECLS
