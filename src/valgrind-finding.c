#include "valgrind-finding.h"

struct _ValgrindFinding
{
  GObject parent_instance;
  
  gint leaked_bytes;
  GPtrArray *stacktrace;
};

G_DEFINE_TYPE (ValgrindFinding, valgrind_finding, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_LEAKED_BYTES,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

ValgrindFinding *
valgrind_finding_new ()
{
  return g_object_new (VALGRIND_TYPE_FINDING,
                       NULL);
}

static void
valgrind_finding_finalize (GObject *object)
{
  ValgrindFinding *self = (ValgrindFinding *)object;
  
  g_ptr_array_free (self->stacktrace, TRUE);

  G_OBJECT_CLASS (valgrind_finding_parent_class)->finalize (object);
}

static void
valgrind_finding_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ValgrindFinding *self = VALGRIND_FINDING (object);

  switch (prop_id)
    {
    case PROP_LEAKED_BYTES:
      g_value_set_int (value, valgrind_finding_get_leaked_bytes (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
valgrind_finding_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ValgrindFinding *self = VALGRIND_FINDING (object);

  switch (prop_id)
    {
    case PROP_LEAKED_BYTES:
      valgrind_finding_set_leaked_bytes (self, g_value_get_int (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
valgrind_finding_class_init (ValgrindFindingClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = valgrind_finding_finalize;
  object_class->get_property = valgrind_finding_get_property;
  object_class->set_property = valgrind_finding_set_property;
  
  
  properties [PROP_LEAKED_BYTES] =
    g_param_spec_int ("leaked-bytes",
                      "LeakedBytes",
                      "LeakedBytes",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE |
                       G_PARAM_STATIC_STRINGS));
  
  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
valgrind_finding_init (ValgrindFinding *self)
{
  self->stacktrace = g_ptr_array_new_with_free_func (g_object_unref);
}

void             
valgrind_finding_set_leaked_bytes (ValgrindFinding *self,
                                   gint             leaked_bytes)
{
  g_return_if_fail (VALGRIND_IS_FINDING (self));
  
  if (self->leaked_bytes != leaked_bytes)
    {
      self->leaked_bytes = leaked_bytes;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LEAKED_BYTES]);  
    }
}

gint             
valgrind_finding_get_leaked_bytes (ValgrindFinding *self)
{
  g_return_val_if_fail (VALGRIND_IS_FINDING (self), 0);
  
  return self->leaked_bytes;
}

void
valgrind_finding_add_stack (ValgrindFinding *self,
                            ValgrindStack   *stack)
{
  g_return_if_fail (VALGRIND_IS_FINDING (self));
  g_return_if_fail (VALGRIND_IS_STACK (stack));
  
  g_ptr_array_add (self->stacktrace, stack);
}

GPtrArray *
valgrind_finding_get_stacktrace (ValgrindFinding *self)
{
  g_return_val_if_fail (VALGRIND_IS_FINDING (self), NULL);
  
  return self->stacktrace;
}
