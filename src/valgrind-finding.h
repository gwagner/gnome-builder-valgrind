#pragma once

#include <glib-object.h>
#include "valgrind-stack.h"

G_BEGIN_DECLS

#define VALGRIND_TYPE_FINDING (valgrind_finding_get_type())

G_DECLARE_FINAL_TYPE (ValgrindFinding, valgrind_finding, VALGRIND, FINDING, GObject)

ValgrindFinding *valgrind_finding_new              ();
void             valgrind_finding_set_leaked_bytes (ValgrindFinding *self,
                                                    gint             leaked_bytes);
gint             valgrind_finding_get_leaked_bytes (ValgrindFinding *self);
void             valgrind_finding_add_stack        (ValgrindFinding *self,
                                                    ValgrindStack   *stack);
GPtrArray       *valgrind_finding_get_stacktrace   (ValgrindFinding *self);

G_END_DECLS
