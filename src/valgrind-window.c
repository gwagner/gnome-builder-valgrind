/* valgrind-window.c
 *
 * Copyright 2021 Günther Wagner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "valgrind-config.h"
#include "valgrind-window.h"
#include "valgrind-reader.h"
#include "valgrind-finding.h"
#include "valgrind-category.h"
#include "valgrind-stack-widget.h"

struct _ValgrindWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkListView         *tree;
};

G_DEFINE_TYPE (ValgrindWindow, valgrind_window, GTK_TYPE_APPLICATION_WINDOW)

static void
valgrind_window_class_init (ValgrindWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/valgrind/valgrind-window.ui");
  gtk_widget_class_bind_template_child (widget_class, ValgrindWindow, tree);
}

static GListModel *
valgrind_create_childs (gpointer item,
                        gpointer user_data)
{
  if (VALGRIND_IS_CATEGORY (item))
    {
      ValgrindCategory *category = VALGRIND_CATEGORY (item);
      GListStore *store = g_list_store_new (VALGRIND_TYPE_FINDING);
      GPtrArray *findings = valgrind_category_get_findings (category);

      for (uint i = 0; i < findings->len; i++)
        {
          g_list_store_append (store, g_ptr_array_index (findings, i));
        }

      return G_LIST_MODEL (store);
    }
  else if (VALGRIND_IS_FINDING (item)) {
    ValgrindFinding *finding = VALGRIND_FINDING (item);
    GPtrArray *stacktrace = valgrind_finding_get_stacktrace (finding);
    
    if (stacktrace->len == 0) return NULL;
    GListStore *store = g_list_store_new (VALGRIND_TYPE_STACK);
    for (uint i = 0; i < stacktrace->len; i++)
      {
        g_list_store_append (store, g_ptr_array_index (stacktrace, i));
      }
    return G_LIST_MODEL (store);
  }
  return NULL;
}

static void
valgrind_setup_widgets (GtkSignalListItemFactory *factory,
                        GtkListItem              *listitem,
                        gpointer                  user_data)
{
  GtkWidget *expander = gtk_tree_expander_new ();
  
  gtk_list_item_set_child (listitem, expander);
}

static void
valgrind_bind_widgets (GtkSignalListItemFactory *factory,
                       GtkListItem              *listitem,
                       gpointer                  user_data)
{
  GtkTreeExpander *expander = GTK_TREE_EXPANDER (gtk_list_item_get_child (listitem));
  GtkTreeListRow *obj = gtk_list_item_get_item (listitem);
  gtk_tree_expander_set_list_row (expander, obj);
  gpointer item = gtk_tree_list_row_get_item (obj);

  if (VALGRIND_IS_CATEGORY (item))
    {
      GtkWidget *lbl = gtk_label_new (NULL);
      gtk_tree_expander_set_child (expander, lbl);

      gtk_label_set_text (GTK_LABEL (lbl), valgrind_category_get_kind (VALGRIND_CATEGORY (item)));
    }
  else if (VALGRIND_IS_FINDING (item))
    {
      GtkWidget *lbl = gtk_label_new (NULL);
      gtk_tree_expander_set_child (expander, lbl);
      
      gchar bytes[100];
      ValgrindFinding *finding = VALGRIND_FINDING (item);
      gint leaked_bytes = valgrind_finding_get_leaked_bytes (finding);
      g_snprintf (bytes, sizeof bytes, "%d Bytes", leaked_bytes);
      
      gtk_label_set_text (GTK_LABEL (lbl), bytes);
    }
  else if (VALGRIND_IS_STACK (item))
    {
      ValgrindStack *stack = VALGRIND_STACK (item);
      
      ValgrindStackWidget *w = valgrind_stack_widget_new (stack);
      gtk_tree_expander_set_child (expander, GTK_WIDGET (w));
    }
}

static void
valgrind_window_init (ValgrindWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  
  g_autoptr(GPtrArray) arr = valgrind_read ();
  
  GListStore *store = g_list_store_new (VALGRIND_TYPE_CATEGORY);
  for (uint i = 0; i < arr->len; i++)
    {
      g_list_store_append (store, g_ptr_array_index (arr, i));
    }
  GtkTreeListModel *model = gtk_tree_list_model_new (G_LIST_MODEL (store), FALSE, FALSE, valgrind_create_childs, NULL, NULL);
  GtkSingleSelection *selection_model = gtk_single_selection_new (G_LIST_MODEL (model));
  
  gtk_list_view_set_model (self->tree, GTK_SELECTION_MODEL (selection_model));
  GtkListItemFactory *factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (valgrind_setup_widgets), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (valgrind_bind_widgets), NULL);
  
  gtk_list_view_set_factory (self->tree, GTK_LIST_ITEM_FACTORY (factory));
}
