#include "valgrind-stack-widget.h"

struct _ValgrindStackWidget
{
  GtkBox parent_instance;
  
  GtkLabel *function;
  GtkLabel *instruction_pointer;
};

G_DEFINE_TYPE (ValgrindStackWidget, valgrind_stack_widget, GTK_TYPE_BOX)

ValgrindStackWidget *
valgrind_stack_widget_new (ValgrindStack *stack)
{
  ValgrindStackWidget *self = g_object_new (VALGRIND_TYPE_STACK_WIDGET, NULL);
  
  valgrind_stack_widget_set_stack (self, stack);
  
  return self;
}

static void
valgrind_stack_widget_finalize (GObject *object)
{
  ValgrindStackWidget *self = (ValgrindStackWidget *)object;

  G_OBJECT_CLASS (valgrind_stack_widget_parent_class)->finalize (object);
}

static void
valgrind_stack_widget_class_init (ValgrindStackWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = valgrind_stack_widget_finalize;
  
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/valgrind/valgrind-stack-widget.ui");
  gtk_widget_class_bind_template_child (widget_class, ValgrindStackWidget, function);  
  gtk_widget_class_bind_template_child (widget_class, ValgrindStackWidget, instruction_pointer);  
}

static void
valgrind_stack_widget_init (ValgrindStackWidget *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

void
valgrind_stack_widget_set_stack (ValgrindStackWidget *self,
                                 ValgrindStack       *stack)
{
  g_return_if_fail (VALGRIND_IS_STACK_WIDGET (self));
  g_return_if_fail (VALGRIND_IS_STACK (stack));
  
  if (valgrind_stack_get_fn (stack) != NULL)
    gtk_label_set_text (self->function, valgrind_stack_get_fn (stack));
  else
    gtk_label_set_text (self->function, valgrind_stack_get_object (stack));

  gtk_label_set_text (self->instruction_pointer, valgrind_stack_get_ip (stack));
}
