/* valgrind-category.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "valgrind-category.h"

struct _ValgrindCategory
{
  GObject parent_instance;

  gchar *kind;
  GPtrArray *findings;
};

G_DEFINE_TYPE (ValgrindCategory, valgrind_category, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_KIND,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

ValgrindCategory *
valgrind_category_new (const gchar *kind)
{
  return g_object_new (VALGRIND_TYPE_CATEGORY,
                       "kind", kind,
                       NULL);
}

static void
valgrind_category_finalize (GObject *object)
{
  ValgrindCategory *self = (ValgrindCategory *)object;

  g_free (self->kind);

  G_OBJECT_CLASS (valgrind_category_parent_class)->finalize (object);
}

static void
valgrind_category_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  ValgrindCategory *self = VALGRIND_CATEGORY (object);

  switch (prop_id)
    {
    case PROP_KIND:
      g_value_set_string (value, valgrind_category_get_kind (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
valgrind_category_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  ValgrindCategory *self = VALGRIND_CATEGORY (object);

  switch (prop_id)
    {
    case PROP_KIND:
      valgrind_category_set_kind (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
valgrind_category_class_init (ValgrindCategoryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = valgrind_category_finalize;
  object_class->get_property = valgrind_category_get_property;
  object_class->set_property = valgrind_category_set_property;

  properties [PROP_KIND] =
    g_param_spec_string ("kind",
                         "Kind",
                         "Kind",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
valgrind_category_init (ValgrindCategory *self)
{
  self->findings = g_ptr_array_new_with_free_func (g_object_unref);
}

void
valgrind_category_set_kind (ValgrindCategory *self,
                            const gchar      *kind)
{
  g_return_if_fail (VALGRIND_IS_CATEGORY (self));

  if (!g_strcmp0 (self->kind, kind) == 0)
    {
      g_free (self->kind);
      self->kind = g_strdup (kind);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_KIND]);
    }
}

const gchar *
valgrind_category_get_kind (ValgrindCategory *self)
{
  g_return_val_if_fail (VALGRIND_IS_CATEGORY (self), NULL);

  return self->kind;
}

void
valgrind_category_add_finding (ValgrindCategory *self,
                               ValgrindFinding  *finding)
{
  g_return_if_fail (VALGRIND_IS_CATEGORY (self));
  g_return_if_fail (VALGRIND_IS_FINDING (finding));

  g_ptr_array_add (self->findings, finding);
}

GPtrArray *
valgrind_category_get_findings (ValgrindCategory *self)
{
  g_return_val_if_fail (VALGRIND_IS_CATEGORY (self), NULL);

  return self->findings;
}
