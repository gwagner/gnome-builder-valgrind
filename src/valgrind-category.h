/* valgrind-category.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "valgrind-finding.h"

G_BEGIN_DECLS

#define VALGRIND_TYPE_CATEGORY (valgrind_category_get_type())

G_DECLARE_FINAL_TYPE (ValgrindCategory, valgrind_category, VALGRIND, CATEGORY, GObject)

ValgrindCategory *valgrind_category_new            (const gchar      *kind);
void              valgrind_category_set_kind       (ValgrindCategory *self,
                                                    const gchar      *kind);
const gchar      *valgrind_category_get_kind       (ValgrindCategory *self);
void              valgrind_category_add_finding    (ValgrindCategory *self,
                                                    ValgrindFinding  *finding);
GPtrArray        *valgrind_category_get_findings   (ValgrindCategory *self);

G_END_DECLS
